/* animation sal */
sal();

/*
// Set the date we're counting down to
const countDownDate = new Date("Dec 11, 2021 16:00:00 GMT+0100").getTime();

const ahora = new Date().getTime();


var updateCounter = function() {

// Update the count down every 1 second
const x = setInterval(function () {

    // Get today's date and time
    const now = new Date().getTime();

    // Find the distance between now and the count down date
    const distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the result in the element with id="demo"
    document.getElementById("countdown").innerHTML = 'Emitimos en ' + days + "d " + hours + "h "
        + minutes + "m " + seconds + "s ";

    // If the count down is finished, write some text
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "¡El evento en directo terminó";
    }
}, 1000);

};



if (countDownDate > ahora ) { 
    updateCounter();
    
}
else {
    document.getElementById("countdown-banner").classList.add('hidden');
}

*/
/*

const player = new Shikwasa({
    container: () => document.querySelector('#player'),
    themeColor: '#ffffff',
    audio: {
      title: 'Evento Programación',
      artist: '24H24L',
      cover: '/images/logo-24h24l.png',
      src: 'https://icecast.hiveagile.dev/24h24l.hifi.mp3', 
      live: true
    },
    fixed: {
        type: 'static',
        position: 'top' 
    }
});

const charlas = [

    {
        "title": "Ya sé programar ¿y ahora qué?",
        "start": new Date("Dec 11, 2021 16:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 11, 2021 16:59:59 GMT+0100").getTime(),
    },
    {
        "title": " Metodologías Ágiles",
        "start": new Date("Dec 11, 2021 17:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 11, 2021 17:59:59 GMT+0100").getTime(),
    },

    {
        "title": "Gestión de equipos",
        "start": new Date("Dec 11, 2021 18:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 11, 2021 18:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Vivir de tu producto",
        "start": new Date("Dec 11, 2021 19:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 11, 2021 19:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Crear una API",
        "start": new Date("Dec 11, 2021 20:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 11, 2021 20:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Desarrollo web ¿Qué debo aprender?",
        "start": new Date("Dec 11, 2021 21:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 11, 2021 21:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Optimización Web",
        "start": new Date("Dec 11, 2021 22:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 11, 2021 22:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Aprendiendo gestores de contenido (CMS)",
        "start": new Date("Dec 11, 2021 23:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 11, 2021 23:59:59 GMT+0100").getTime(),
    },
    {
        "title": "El futuro de JavaScript",
        "start": new Date("Dec 12, 2021 00:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 00:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Framework de JavaScript",
        "start": new Date("Dec 12, 2021 01:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 01:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Diseño UX",
        "start": new Date("Dec 12, 2021 02:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 02:59:59 GMT+0100").getTime(),
    },
    {
        "title": "¿Por qué aprender Python?",
        "start": new Date("Dec 12, 2021 03:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 03:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Desarrollo con n8n",
        "start": new Date("Dec 12, 2021 04:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 04:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Herramientas de despliegue",
        "start": new Date("Dec 12, 2021 05:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 05:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Big Data",
        "start": new Date("Dec 12, 2021 06:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 06:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Programación funcional",
        "start": new Date("Dec 12, 2021 07:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 07:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Programación y microservicios",
        "start": new Date("Dec 12, 2021 08:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 08:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Programación con microcontroladores",
        "start": new Date("Dec 12, 2021 09:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 09:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Programación y seguridad",
        "start": new Date("Dec 12, 2021 10:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 10:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Programación con Odoo",
        "start": new Date("Dec 12, 2021 11:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 11:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Programación en BIM",
        "start": new Date("Dec 12, 2021 12:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 12:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Programación en escritorio",
        "start": new Date("Dec 12, 2021 13:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 13:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Programación GIS",
        "start": new Date("Dec 12, 2021 14:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 14:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Programación en videojuegos",
        "start": new Date("Dec 12, 2021 15:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 15:59:59 GMT+0100").getTime(),
    },
    {
        "title": "Mesa Redonda",
        "start": new Date("Dec 12, 2021 16:00:00 GMT+0100").getTime(),
        "end": new Date("Dec 12, 2021 18:00:59 GMT+0100").getTime(),
    }
  
];

const up = setInterval(function () {


    var title = 'Evento Programación';


    var date = new Date(); 
    var now_utc =  Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    var now = new Date(now_utc).getTime();


    charlas.forEach(charla => {
    
        if( (now >= charla.start ) && (now <= charla.end)){
            title = charla.title;
        }

    });
    
    document.querySelector('span.shk-title').innerHTML = title;

}, 15000);

*/
